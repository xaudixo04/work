from django.db import models

import pytz
from datetime import date


class Client(models.Model):

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField('Номер телефона', max_length=7)
    operator_code = models.CharField(max_length=3)
    tag = models.CharField('Тег', max_length=255)
    time_zone = models.CharField('Часовой пояс', max_length=32, choices=TIMEZONES)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):

    created_at = models.DateTimeField('Время создания', auto_now_add=True)
    status = models.BooleanField(default=True)
    text = models.TextField('Текст сообщения')

    def __str__(self):
        return self.text
    
    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class Mailing(models.Model):

    start_date = models.DateField(auto_created=True, default=date.today, verbose_name='Дата старта', editable=True)
    end_date = models.DateField(auto_created=True, default=date.today, verbose_name='Дата окончания', editable=True)
    message = models.ForeignKey(Message, verbose_name='Сообщение', on_delete=models.PROTECT)
    clients = models.ManyToManyField(Client, verbose_name='Клиенты', related_name='mails')

    def __str__(self):
        return str(self.id)
    
    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'