from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from .models import Client, Message, Mailing
from .serializers import ClientSerializer, MessageSerializer, MailingSerializer


factory = APIRequestFactory()
request = factory.get('/')


serializer_context = {
    'request': Request(request),
}


@api_view(['GET'])
def get_all_clients(request):
    clients = Client.objects.all()
    serializer = ClientSerializer(clients, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_client(request, user_id):
    client = Client.objects.get(pk=user_id)
    serializer = ClientSerializer(client, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def create_user(request):
    data = request.data
    client = Client.objects.create(
        phone_number=data['phone_number'],
        operator_code=data['operator_code'],
        tag=data['tag'],
        time_zone=data['time_zone']

        )
    serializer = ClientSerializer(client, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def delete_client(request, user_id):
    client = Client.objects.delete(pk=user_id)
    serializer = ClientSerializer(client, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def get_all_messages(request):
    messages = Message.objects.all()
    serializer = MessageSerializer(messages, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_message(request, message_id):
    message = Message.objects.get(pk=message_id)
    serializer = MessageSerializer(message, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def create_message(request):
    data = request.data
    client = Message.objects.create(
        status=data['status'],
        text=data['text']
        )
    serializer = MessageSerializer(client, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def delete_message(request, message_id):
    client = Message.objects.delete(pk=message_id)
    serializer = MessageSerializer(client, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def get_all_mailings(request):
    mailings = Mailing.objects.all()
    serializer = MailingSerializer(mailings, context={'request': request}, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_mailing(request, mailing_id):
    mailing = Mailing.objects.get(pk=mailing_id)
    serializer = MailingSerializer(mailing, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def create_mailing(request):
    data = request.data
    mailing = Mailing.objects.create(
        start_date=data['start_date'],
        end_date=data['end_date'],
        message_id=data['message_id'],
        clients=data['clients']
        )
    serializer = MailingSerializer(mailing, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def delete_mailing(request, mailing_id):
    mailing = Mailing.objects.delete(pk=mailing_id)
    serializer = MailingSerializer(mailing, many=False)
    return Response(serializer.data)