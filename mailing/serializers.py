from rest_framework import serializers

from .models import Client, Message, Mailing


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'operator_code', 'tag', 'time_zone']


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'created_at', 'status', 'text']


class MailingSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Mailing
        fields = ['id', 'start_date', 'end_date', 'message_id']