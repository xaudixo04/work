from django.urls import path, include

from .views import *


urlpatterns = [
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('clients/', get_all_clients),
    path('clients/<int:user_id>/', get_client),
    path('clients/<int:user_id>/delete', delete_client),

    path('messages/', get_all_messages),
    path('messages/<int:message_id>/', get_message),
    path('messages/<int:message_id>/delete', delete_message),

    path('mailings/', get_all_mailings),
    path('mailings/<int:mailing_id>/', get_mailing),
    path('mailings/<int:mailing_id>/delete', delete_mailing),
]